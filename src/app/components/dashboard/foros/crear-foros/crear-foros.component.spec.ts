import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearForosComponent } from './crear-foros.component';

describe('CrearForosComponent', () => {
  let component: CrearForosComponent;
  let fixture: ComponentFixture<CrearForosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearForosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearForosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
